# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.transaction import Transaction


class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    @fields.depends('company', '_parent_invoice.company',
        '_parent_invoice.party', 'party', 'invoice')
    def on_change_product(self):
        party = None
        if self.invoice and self.invoice.party:
            party = self.invoice.party
        elif self.party:
            party = self.party

        company = self.company or (self.invoice and self.invoice.company)
        partner = (party in company.party.partners if company else None)
        with Transaction().set_context(partner=partner):
            super().on_change_product()
