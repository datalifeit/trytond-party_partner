# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.transaction import Transaction


class PurchaseLine(metaclass=PoolMeta):
    __name__ = 'purchase.line'

    def get_invoice_line(self):
        party = None
        if self.purchase and self.purchase.party:
            party = self.purchase.party
        elif self.party:
            party = self.party

        company = self.purchase.company
        partner = (party in company.party.partners)
        with Transaction().set_context(partner=partner):
            return super().get_invoice_line()
