# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
import unittest
import doctest
import trytond.tests.test_tryton
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.tests.test_tryton import doctest_teardown
from trytond.tests.test_tryton import doctest_checker


class PartnerTestCase(ModuleTestCase):
    """Test party partner module"""
    module = 'party_partner'

    @with_transaction()
    def test_party(self):
        """Create parties"""
        pool = Pool()
        Party = pool.get('party.party')

        party1, party2 = Party.create([
            {'name': 'Party 1'},
            {'name': 'Party 2'},
        ])
        Party.write([party1],
                    {'partners': [('add', [party2.id])]})
        self.assert_(party1.partners)
        self.assertEqual(party2.societies[0], party1)


def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader(
        ).loadTestsFromTestCase(PartnerTestCase))
    return suite
