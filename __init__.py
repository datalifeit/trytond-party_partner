# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import party
from . import company
from . import invoice
from . import sale
from . import purchase


def register():
    Pool.register(
        party.PartyPartner,
        party.Party,
        party.PartyAccount,
        company.Company,
        module='party_partner', type_='model')
    Pool.register(
        invoice.InvoiceLine,
        module='party_partner', type_='model',
        depends=['account_invoice'])
    Pool.register(
        sale.SaleLine,
        module='party_partner', type_='model',
        depends=['sale'])
    Pool.register(
        purchase.PurchaseLine,
        module='party_partner', type_='model',
        depends=['purchase'])
