# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from simpleeval import simple_eval
from trytond.model import ModelSQL, fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.tools import decistmt
from trytond.exceptions import UserError
from trytond.i18n import gettext


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'

    partners = fields.Many2Many('party.party.partner',
                                'from_', 'to_', 'Partners')
    societies = fields.Many2Many('party.party.partner',
                                 'to_', 'from_', 'Societies')
    share_capital_formula = fields.Char('share_capital formula',
                                help=('Python expression that will be '
                                    'evaluated with:\n'
                                  '- untaxed_amount: untaxed amount\n'
                                  '- total_amount: total amount\n'
                                  '- quantity: quantity\n'))
    account_share_capital = fields.MultiValue(
            fields.Many2One('account.account', 'Account Subsidy',
                            domain=[('kind', '=', 'other'),
                                    ('company', '=', Eval(
                                        'context', {}).get('company', -1))])
    )

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field == 'account_share_capital':
            return pool.get('party.party.account')
        return super(Party, cls).multivalue_model(field)

    def check_share_capital_formula(self):
        context = self.get_context_share_capital_formula(field_values={})

        try:
            value = self.get_share_capital_amount(**context)
            if not isinstance(value, Decimal) and not isinstance(
                    Decimal(value), Decimal):
                raise ValueError
        except ValueError as exception:
            raise UserError(gettext(
                'party_partner.msg_invalid_share_capital_formula',
                formula=self.share_capital_formula,
                party=self.rec_name,
                exception=exception))

    def get_context_share_capital_formula(self, **field_values):
        return {'names': {
                'untaxed_amount': field_values.get(
                    'untaxed_amount', Decimal('0.0')),
                'total_amount': field_values.get(
                    'total_amount', Decimal('0.0')),
                'quantity': field_values.get('quantity', Decimal('0.0'))
                },
        }

    def get_share_capital_amount(self, **field_values):
        """Return share_capital amount (as Decimal)"""
        if not self.share_capital_formula:
            return Decimal('0.0')
        context = self.get_context_share_capital_formula(**field_values)
        context.setdefault('functions', {})['Decimal'] = Decimal
        return simple_eval(decistmt(self.share_capital_formula), **context)

    @classmethod
    def validate(cls, records):
        super(Party, cls).validate(records)
        for record in records:
            record.check_share_capital_formula()


class PartyAccount(metaclass=PoolMeta):
    __name__ = 'party.party.account'

    account_share_capital = fields.Many2One(
        'account.account', 'Account Subsidy',
        domain=[('kind', '=', 'other'),
                ('company', '=', Eval(
                    'context', {}).get('company', -1))])

    @classmethod
    def __register__(cls, module_name):
        table_h = cls.__table_handler__(module_name)
        exist = table_h.table_exist(cls._table)
        if exist:
            exist &= table_h.column_exist('account_share_capital')

        super(PartyAccount, cls).__register__(module_name)

        if not exist:
            # Re-migration
            cls._migrate_property([], [], [])

    @classmethod
    def _migrate_property(cls, field_names, value_names, fields):
        field_names.append('account_share_capital')
        value_names.append('account_share_capital')
        super(PartyAccount, cls)._migrate_property(field_names,
            value_names, fields)


class PartyPartner(ModelSQL):
    """Party partner"""
    __name__ = 'party.party.partner'

    from_ = fields.Many2One('party.party', 'From',
                            required=True, select=True,
                            ondelete='CASCADE')
    to_ = fields.Many2One('party.party', 'To',
                          required=True, select=True,
                          ondelete='CASCADE')
